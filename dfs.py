def main():
    graph_example = {
        'A': {'B', 'F'},
        'B': {'A'},
        'C': {'D', 'E', 'F'},
        'D': {'C', 'G'},
        'E': {'C'},
        'F': {'C', 'G', 'A'},
        'G': {'F', 'D'},
    }

    start_node = 'B'
    end_node = 'E'
    print(f'Some possible ways from {start_node} to {end_node}:')
    for i in depth_search(graph_example, [], start_node, end_node):
        print(i)


def depth_search(graph, visited, start_node, end_node):
    if len(visited) == 0:
        visited = [start_node]
    if start_node == end_node:
        yield visited
    for node in graph[start_node] - set(visited):
        # uncomment to see the traversal path:
        # print(node + ' (visited: ' + str(visited) + ')')
        yield from depth_search(graph, visited + [node], node, end_node)


if __name__ == '__main__':
    main()

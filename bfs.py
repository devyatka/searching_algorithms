def main():
    graph_example = {
        'A': {'B', 'F'},
        'B': {'A'},
        'C': {'D', 'E', 'F'},
        'D': {'C', 'G'},
        'E': {'C'},
        'F': {'C', 'G', 'A'},
        'G': {'F', 'D'},
    }

    start_node = 'B'
    end_node = 'E'
    print(f'Some possible ways from {start_node} to {end_node}:')
    for i in breadth_search(graph_example, start_node, end_node):
        print(i)


def breadth_search(graph, start_node, end_node):
    opened = [(start_node, [start_node])]
    while opened:
        (vertex, path) = opened.pop(0)
        for node in graph[vertex] - set(path):
            # uncomment to see the traversal path:
            # print(node + ' (path to this node: ' + str(path + [node]) + ')')
            if node == end_node:
                yield path + [node]
            else:
                opened.append((node, path + [node]))


if __name__ == '__main__':
    main()
